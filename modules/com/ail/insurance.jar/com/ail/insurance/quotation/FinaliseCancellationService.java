/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
package com.ail.insurance.quotation;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceInterface;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;

@ServiceInterface
public interface FinaliseCancellationService {

    @ServiceArgument
    public interface FinaliseCancellationArgument extends Argument {
        void setPolicyArg(Policy policyArg);

        Policy getPolicyArg();

        void setQuotationArg(Policy quotationArg);

        Policy getQuotationArg();
    }

    @ServiceCommand
    public interface FinaliseCancellationCommand extends Command, FinaliseCancellationArgument {
    }
}
