/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core.audit.envers;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.core.persistence.hibernate.HibernateSessionBuilder.getSessionFactory;
import static org.hibernate.envers.RevisionType.ADD;
import static org.hibernate.envers.query.AuditEntity.id;
import static org.hibernate.envers.query.AuditEntity.revisionType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;

import com.ail.annotation.ServiceImplementation;
import com.ail.core.NotImplementedError;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.Type;
import com.ail.core.audit.AuditRoot;
import com.ail.core.audit.FetchRevisionsService.FetchRevisionsArgument;
import com.ail.core.persistence.CreateException;

/**
 * An implementation of FetchRevisionsService for Envers.
 */
@ServiceImplementation
public class EnversFetchRevisionsService extends Service<FetchRevisionsArgument> {

    @Override
    public void invoke() throws PreconditionException, CreateException {
        args.setRevisionsRet(new ArrayList<>());

        if (args.getTypeArg() instanceof AuditRoot) {
            populateRetForAuditRootType();
        }
        else {
            populateRetForNormalType();
        }
    }

    private void populateRetForAuditRootType() {
        AuditReader auditReader = AuditReaderFactory.get(getSessionFactory().getCurrentSession());

        addRevisionForCreation(auditReader);

        addRevisionsForPolicyRequests(auditReader);
    }

    private void addRevisionForCreation(AuditReader auditReader) {
        Object[] queryResult = (Object[]) auditReader.
                                    createQuery().
                                    forRevisionsOfEntity(args.getTypeArg().getClass(), false, true).
                                    add(id().eq(args.getTypeArg().getSystemId())).
                                    add(revisionType().eq(ADD)).
                                    getSingleResult();

        int revisionId = ((Revision)queryResult[1]).getId();

        // Evict the proxy object which hibernate will have created for the query above. If we
        // don't, it'll be cached and reused when the revisions are loaded which causes problems
        // in envers' findEntitiesGroupByRevisionType method.
        getSessionFactory().getCurrentSession().evict(queryResult[1]);

        Revision revision = (Revision)getCoreProxy().queryUnique("get.revision.by.id", revisionId);
        addRevisionToResults(auditReader, revision);
    }

    private void addRevisionsForPolicyRequests(AuditReader auditReader) {
        List<Revision> revisions = fetchRevisionsByProvidedId();

        for(Revision revision: revisions) {
            // Evict the proxy object which hibernate will have created for the query above. If we
            // don't, it'll be cached and reused when the revisions are loaded which causes problems
            // in envers' findEntitiesGroupByRevisionType method.
            getSessionFactory().getCurrentSession().evict(revision);
            addRevisionToResults(auditReader, revision);
        }
    }

    @SuppressWarnings("unchecked")
    private List<Revision> fetchRevisionsByProvidedId() {
        String externalPolicyId = args.getTypeArg().getExternalSystemId() != null
                                    ? args.getTypeArg().getExternalSystemId()
                                    : fetchExternalIdForSystemId(args.getTypeArg().getSystemId());

        return (List<Revision>)getCoreProxy().query("get.revisions.by.external.policy.id", externalPolicyId);
    }

    private String fetchExternalIdForSystemId(long systemId) {
        return ((Type)getCore().queryUnique("get.policy.by.systemId", systemId)).getExternalSystemId();
    }

    void addRevisionToResults(AuditReader auditReader, Revision revision) {
        Map<RevisionType, List<Object>> changes = auditReader.getCrossTypeRevisionChangesReader().findEntitiesGroupByRevisionType(revision.getId());
        args.getRevisionsRet().add(new com.ail.core.audit.Revision(revision, convertObjectsToTypes(changes)));
    }

    private Map<RevisionType, List<Type>> convertObjectsToTypes(Map<RevisionType, List<Object>> source) {
        Map<RevisionType, List<Type>> result = new HashMap<>();

        for(RevisionType revisionType: source.keySet()) {
            result.put(revisionType,
                       source.get(revisionType).stream().
                           filter(i -> i instanceof Type).
                           map(i -> (Type)i).
                           collect(Collectors.toList())
                      );
        }

        return result;
    }

    private void populateRetForNormalType() {
//        AuditReader auditReader = AuditReaderFactory.get(getSessionFactory().getCurrentSession());
//
//        @SuppressWarnings("unchecked")
//        List<Object[]> revisions = auditReader.
//                            createQuery().
//                            forRevisionsOfEntity(args.getTypeArg().getClass(), false, true).
//                            add(id().eq(args.getTypeArg().getSystemId())).
//                            getResultList();
//
//        for(Object[] res: revisions) {
//            args.getRevisionsRet().add(new com.ail.core.audit.Revision((Revision)res[1], (Type) res[0], toRevisionType(res[2])));
//        }
        throw new NotImplementedError("EnversFetchRevisionsService.populateRetForNormalType");
    }
}
