/* Copyright Applied Industrial Logic Limited 2018. All rights reserved. */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core.audit;

import com.ail.core.logging.ServiceRequestRecord;

/**
 * <p>
 * An AuditRoot class is one whose audit history should include the details of
 * changes to any entity that it has a relationship with, not just the entity
 * itself.
 * </p>
 * <p>
 * When the history of an normal entity (one that doesn't implement AuditRoot)
 * is queried the history will only detail transactions in which the entity
 * itself was involved. For example, if a system were auditing a Policy entity
 * and the Party entities within it (client, broker, etc.) the history of the
 * Policy object would not include any reference to changes that were made to
 * Party properties like names, addresses, etc. as they are audited by Party
 * only. The Policy entity's history would only include the details of changes
 * to the properties directly only the Policy.
 * </p>
 * <p>The audit services handle AuditRoot entities differently. In this case the
 * services use the link between {@link Revision} and {@link ServiceRequestRecord}
 * to determine all of the entities that were changed under requests for an entity
 * regardless of whether the entity itself was modified.</p>
 * @see FetchRevisionNumbersService
 * @see FetchRevisionService
 * @see FetchRevisionsService
 */
public interface AuditRoot {
    /**
     * Indicate whether this instance of the implementing class was loaded from history.
     * @return Return true if this instance of the implementing class was loaded from
     * history, false otherwise.
     */
    boolean isAuditRecord();

    /**
     * @see #isAuditRecord()
     * @param auditRecord
     */
    void setAuditRecord(boolean auditRecord);
}
