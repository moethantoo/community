package com.ail.base;

import com.ail.core.product.ProductServiceCommand;
import com.ail.insurance.quotation.FinaliseMTAService.FinaliseMTAArgument;

/**
 * Apply the changes made in an MTA quotation to the master policy.
 * <p>
 * This default implementation does nothing as the core system's
 * {@link com.ail.insurance.quotation.ApplyMTAQuotationService
 * ApplyMTAQuotationService} has already taken care of all the standard merging
 * steps before it invokes this product specific service. Product specific
 * implementation of this service are expected to deal with updating the master
 * policy's assets, sections, coverages etc. wrt the MTA quotation.
 * </p>
 * <p>
 * Essentially, product specific implementation will update
 * {@link FinaliseMTAArgument#getMTAPolicyArg()} with respect to values taken
 * from {@link FinaliseMTAArgument#getMTAQuotationArg()}.
 * </p>
 */
@ProductServiceCommand(serviceName = "FinaliseMTAService", commandName = "FinaliseMTA")
public class FinaliseMTAService {

	public static void invoke(FinaliseMTAArgument args) {
	}
}