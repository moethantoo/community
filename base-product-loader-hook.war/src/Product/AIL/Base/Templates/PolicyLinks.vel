## Copyright Applied Industrial Logic Limited 2012. All rights Reserved 
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 2 of the License, or (at your option) any later 
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
## FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
## more details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, write to the Free Software Foundation, Inc., 51 
## Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
##
#####
#set( $quote = $args.ModelArgRet)
#set( $pageElement = $args.PageElementArg )
#set( $policy = $args.PolicyArg )
#set( $response = $args.ResponseArgRet )
#set( $renderId = $args.RenderIdArg )
#set( $string = '' )
#set( $q = '"' )
#set( $dateFormat = '%1$ta %1$tb %1$te %1$tY %1$tR' )
##
<div class='pf-policy-links' id='$!{renderId}'>
	<div class='pf-table'>
		<div class='pf-table-header-row'>
			<div>Type</div>
			<div>Id</div>
			<div>Number</div>
			<div>Status</div>
			<div>Created Date</div>
			<div>Updated Date</div>
			<div>&nbsp</div>
		</div>
		#foreach($policyLink in ${pageElement.fetchPolicyLinks($policy)})
			#set($targetPolicy = ${pageElement.fetchLinkedPolicy($policyLink)})
			<div class='pf-table-data-row'>
				<div>#linkType($policyLink)</div>
				<div>#linkTarget($policyLink)</div>
				<div>#targetIdNumber($targetPolicy)</div>
				<div>#targetStatus($targetPolicy)</div>
				<div>#targetCreatedDate($targetPolicy)</div>
				<div>#targetUpdatedDate($targetPolicy)</div>
				<div>#buttons($policyLink $targetPolicy ${foreach.index})</div>
			</div>
		#end
	</div>
</div>
##
#macro(linkType $policyLink)#i18n(${policyLink.LinkType.LongName})#end
##
#macro(linkTarget $policyLink)${policyLink.TargetPolicyId}#end
##
#macro(targetStatus $targetPolicy)
	#if($targetPolicy)
		#i18n(${targetPolicy.Status.LongName})
	#else
		#i18n("i18n_Unknown")
	#end
#end
##
#macro(targetIdNumber $targetPolicy)
	#if(!$targetPolicy)
		#i18n("i18n_Unknown")
	#elseif($targetPolicy.PolicyNumber)
		${targetPolicy.PolicyNumber}
	#elseif($targetPolicy.QuotationNumber)
		${targetPolicy.QuotationNumber}
	#else
		#i18n("i18n_Unknown")
	#end
#end
##
#macro(targetCreatedDate $targetPolicy)
	#if(!$targetPolicy)
		#i18n("i18n_Unknown")
	#else
		$string.format($dateFormat, $targetPolicy.CreatedDate)
	#end
#end
##
#macro(targetUpdatedDate $targetPolicy)
	#if(!$targetPolicy)
		#i18n("i18n_Unknown")
	#else
		$string.format($dateFormat, $targetPolicy.UpdatedDate)
	#end
#end
##
#macro(buttons $policyLink $targetPolicy $row)
	#set($rowIndex = $row) ## Dereference $row. Using $row in the foreach below will render as $foreach.index! 
	#if($targetPolicy)
		#foreach($button in ${pageElement.fetchButtons($policyLink.LinkType, $targetPolicy.Status)})
			#if($button == "i18n_policy_link_button_open")
				#set($onclick = "openPolicyInPageFlow('${response.createResourceURL()}', '${targetPolicy.SystemId}', 'PolicySummaryPageFlow')")
			#else
				#set($onclick = "callServeResource('${response.createResourceURL()}', 'op=${button}:id=${renderId}:row=${rowIndex}')")
			#end
			<input type='button' value='#i18n(${button})' class='portlet-form-input-field'  onclick="${onclick}"/>
		#end
	#end
#end
##
#macro(i18n $id)$pageElement.i18n("$id")#end
