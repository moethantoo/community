package com.ail.service.product;

import static com.ail.core.Functions.productNameToConfigurationNamespace;
import static java.lang.String.format;
import static org.springframework.util.StringUtils.isEmpty;

import com.ail.core.PreconditionException;
import com.ail.core.product.service.ServiceRequestUrl;

public class ServiceRequestUrlImpl implements ServiceRequestUrl {
    private String url;

    ServiceRequestUrlImpl(String url) throws PreconditionException {
        if (url == null) {
            throw new PreconditionException("Request url is null");
        }

        this.url = url;
    }

    @Override
    public String resolveServiceName() throws PreconditionException {
        String[] splitUrl = url.split("/");

        if (splitUrl.length < 3) {
            throw new PreconditionException(format("Request url is malformed: %s", url));
        }

        return splitUrl[splitUrl.length - 1];
    }

    @Override
    public String resolveNamespace() throws PreconditionException {
        String[] splitUrl = url.split("/");

        if (splitUrl.length < 3) {
            throw new PreconditionException(format("Request url is malformed: %s", url));
        }

        String product = splitUrl[0];

        if (isEmpty(product)) {
            product = splitUrl[1];
        }

        return productNameToConfigurationNamespace(product);
    }
}
