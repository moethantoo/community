package com.ail.insurance.search.hibernate;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.Core;
import com.ail.core.security.FilterListAccessibilityToUserService.FilterListAccessibilityToUserCommand;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.search.PolicySearchService.PolicySearchArgument;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ UserLocalServiceUtil.class})
public class HibernatePolicySearchServiceTest {

    HibernatePolicySearchService sut;

    @Mock
    PolicySearchArgument args;
    @Mock
    User user;
    @Mock
    Core core;
    @Mock
    Criteria criteria;
    @Mock
    Policy result1;
    @Mock
    Policy result2;
    @Mock
    List<Policy> serviceResult;
    @Mock
    FilterListAccessibilityToUserCommand filterListAccessibilityToUserCommand;

    List<Object> criteriaResult;
    Collection<Policy> filteredList;

    @Before
    public void setup() throws PortalException, SystemException {
        MockitoAnnotations.initMocks(this);

        sut = spy(new HibernatePolicySearchService());
        sut.setArgs(args);
        sut.setCore(core);

        criteriaResult = asList((Object)result1, (Object)result2);
        filteredList = asList(result1, result2);

        mockStatic(UserLocalServiceUtil.class);

        doReturn(true).when(args).getIncludeSupersededArg();
        doReturn(criteria).when(sut).createCriteria();
        doReturn(criteriaResult).when(criteria).list();
        doReturn(serviceResult).when(args).getPoliciesRet();
        doReturn(filterListAccessibilityToUserCommand).when(core).newCommand(eq("FilterListAccessibilityToUserCommand"),eq(FilterListAccessibilityToUserCommand.class));
        doReturn(filteredList).when(filterListAccessibilityToUserCommand).getListRet();

        when(UserLocalServiceUtil.getUser(anyLong())).thenReturn(user);
    }

    @Test
    public void checkThatConfigurationNamespaceIsSetCorrectly() {
        assertThat(sut.getConfigurationNamespace(), is("AIL.Base.Registry"));
    }

    @Test
    public void checkThatQueryResultIsPassedIntoFilter() throws BaseException {
        sut.invoke();
        verify(filterListAccessibilityToUserCommand).setListArg(eq(criteriaResult));
    }

    @Test
    public void checkThatFilterResultIsPassedBackAsServiceResult() throws BaseException {
        sut.invoke();
        verify(args).setPoliciesRet(eq(filteredList));
    }
}
