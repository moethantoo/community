package com.ail.core;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.junit.Test;

public class DateTimeUtilsTest {

    @Test
    public void testUnitsBetween() {
        Date start = DateTimeUtils.LocalDateTimeToDateUTC(LocalDateTime.of(2016, 1, 5, 13, 45, 23));
        Date end = DateTimeUtils.LocalDateTimeToDateUTC(LocalDateTime.of(2018, 8, 17, 8, 22, 59));

        assert(DateTimeUtils.unitsBetween(start, end, ChronoUnit.YEARS) == 2);
        assert(DateTimeUtils.unitsBetween(start, end, ChronoUnit.MONTHS) == 31);
        assert(DateTimeUtils.unitsBetween(start, end, ChronoUnit.DAYS) == 954);
        assert(DateTimeUtils.unitsBetween(start, end, ChronoUnit.HOURS) == 22914);
        assert(DateTimeUtils.unitsBetween(start, end, ChronoUnit.MINUTES) == 1374877);
        assert(DateTimeUtils.unitsBetween(start, end, ChronoUnit.SECONDS) == 82492656);
    }

    @Test
    public void testFormatDateString() {
        String s = DateTimeUtils.format("2018-09-01", "yyyy-MM-dd", "d MMMMM yyyy");
        assert(s.equals("1 September 2018"));
    }

    @Test
    public void testFormatDateAttribute() {
        Attribute a = new Attribute("date", "2018-09-01", "date,pattern=yyyy-MM-dd");
        String s = DateTimeUtils.formatDateAttribute(a, "d MMMMM yyyy");
        assert(s.equals("1 September 2018"));
    }

    @Test
    public void testAddOrdinalIndicator() {
        String dateIn = "1 September 2018";
        String dateOut = DateTimeUtils.addOrdinalIndicator(dateIn);
        assert(dateOut.equals("1st September 2018"));

        dateIn = "September 1 2018";
        dateOut = DateTimeUtils.addOrdinalIndicator(dateIn);
        assert(dateOut.equals("September 1st 2018"));

        dateIn = "2 September 2018";
        dateOut = DateTimeUtils.addOrdinalIndicator(dateIn);
        assert(dateOut.equals("2nd September 2018"));

        dateIn = "23 September 2018";
        dateOut = DateTimeUtils.addOrdinalIndicator(dateIn);
        assert(dateOut.equals("23rd September 2018"));

        dateIn = "29 September 2018";
        dateOut = DateTimeUtils.addOrdinalIndicator(dateIn);
        assert(dateOut.equals("29th September 2018"));
    }

}
