/* Copyright Applied Industrial Logic Limited 2002. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.financial;

import static com.ail.financial.PaymentRecordType.NEW;
import static org.apache.commons.lang3.StringUtils.repeat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;

import java.math.BigDecimal;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ail.core.Attribute;

public class PaymentRecordTest {

    private PaymentRecord sut;

    private String transactionReference="TRANSACTION_ID";
    @Mock
    private CurrencyAmount currencyAmount;
    @Mock
    private PaymentMethod paymentMethod;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        doReturn("METHOD_ID").when(paymentMethod).getId();
        doReturn(Currency.GBP).when(currencyAmount).getCurrency();
        doReturn(new BigDecimal(20)).when(currencyAmount).getAmount();
    }

    @Test
    public void testConstructor() {
        sut = new PaymentRecord(currencyAmount, transactionReference, paymentMethod, NEW);
        assertEquals(NEW, sut.getType());
        assertEquals("METHOD_ID", sut.getMethodIdentifier());
        assertEquals(transactionReference, sut.getTransactionReference());
    }

    @Test
    public void testConstructorTruncatesDescription() {
        sut = new PaymentRecord(currencyAmount, transactionReference, paymentMethod, NEW, repeat('T', 5));
        assertThat(sut.getDescription(), length(is(5)));

        sut = new PaymentRecord(currencyAmount, transactionReference, paymentMethod, NEW, repeat('T', 256));
        assertThat(sut.getDescription(), length(is(255)));

        sut = new PaymentRecord(currencyAmount, transactionReference, paymentMethod, NEW, repeat('T', 1000));
        assertThat(sut.getDescription(), length(is(255)));
    }

    @Test
    public void testAttributesOnPaymentRecordsAreCloned() throws CloneNotSupportedException {
        sut = new PaymentRecord(currencyAmount, transactionReference, paymentMethod, NEW, repeat('T', 5));
        sut.getAttribute().add(new Attribute("id", "value", "string"));

        PaymentRecord clone = (PaymentRecord)sut.clone();

        assertThat(clone.getAttribute().size(), is(1));
        assertThat(clone.getAttribute().get(0), is(sut.getAttribute().get(0)));
    }

    private static  Matcher<String> length(Matcher<? super Integer> matcher) {
        return new FeatureMatcher<String, Integer>(matcher, "a String of length that", "length") {
            @Override
            protected Integer featureValueOf(String actual) {
              return actual.length();
            }
        };
    }
}
